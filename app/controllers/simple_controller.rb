class SimpleController < ApplicationController
  require 'nokogiri'
  require 'open-uri'

  def scrap
    doc = Nokogiri::HTML(open('https://blog.tokopedia.com/category/promo/'))
    
    promos = doc.css("ul.entry-list li article").first(6)[0]
    byebug
    
    promos.each do |p|
      title       = promos.at_css('h2').content
      description = promos.at_css('p').content
      image_url   = promos.at_css('a img').attr('data-lazy-src')
      expire_date = promos.at_css('span.date').content
      url         = promos.at_css('a').attr('href')
    end
  end
end
