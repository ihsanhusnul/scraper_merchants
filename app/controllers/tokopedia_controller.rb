class TokopediaController < ApplicationController
  require 'nokogiri'
  require 'open-uri'

	def doScrap
    merchant = Merchant.where(name: self.name.slice("Controller")).first

		doc = Nokogiri::HTML(open(merchant.url))

		htmlPromos = doc.css("ul.entry-list li article").first(6)

		htmlPromos.each do |p|
      newPromo = merchant.promos.new
			newPromo.title       = p.at_css('h2').text.strip
			newPromo.description = p.at_css('p').text
			newPromo.image_url   = p.at_css('a img').attr('data-lazy-src')
			newPromo.expire_date = p.at_css('span.date').text
			newPromo.url         = p.at_css('a').attr('href')

      if newPromo.save
        puts "Save '#{newPromo.title}' Successfully!"
      else
        puts "Save '#{newPromo.title}' Failed!"
      end
    end
	end
end
