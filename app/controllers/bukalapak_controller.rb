class BukalapakController < ApplicationController
	require 'nokogiri'
  require 'open-uri'

  def doScrap
    puts self.class.name.chomp("Controller")
    merchant = Merchant.where(name: self.class.name.chomp("Controller")).first

    doc = Nokogiri::HTML(open(merchant.url))

    htmlPromos = doc.css('.td-ss-main-content > .td_module_11')

    htmlPromos.each do |p|
      newPromo = merchant.promos.new
      newPromo.title       = p.at_css('.entry-title').text
      newPromo.description = p.at_css('.td-excerpt').text.strip
      newPromo.image_url   = p.at_css('.entry-thumb/@src').value
      newPromo.expire_date = p.at_css('.entry-date/@datetime').value
      newPromo.url         = p.at_css('div[@class=td-module-thumb]/a/@href').value

      if newPromo.save
        puts "Save '#{newPromo.title}' Successfully!"
      else
        puts "Save '#{newPromo.title}' Failed!"
      end
    end
  end
end
