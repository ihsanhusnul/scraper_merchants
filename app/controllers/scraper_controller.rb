class ScraperController < ApplicationController
  require 'nokogiri'
  require 'open-uri'

  def doScrapers
    merchants = Merchant.where('isActive = ?', true)

    self.scrapersInBackground merchants
  end

  def scrapersInBackground merchants
    # Thread.new do
      merchants.each do |m|
        m.doScraper
      end

      # ActiveRecord::Base.connection.close
    # end
  end
end
