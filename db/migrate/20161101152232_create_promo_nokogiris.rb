class CreatePromoNokogiris < ActiveRecord::Migration
  def change
    create_table :promo_nokogiris do |t|

    	t.string :tag_image
      t.string :tag_expire_date
      t.string :tag_description
      t.string :tag_title
      t.integer :merchant_id
      t.timestamps null: false
    end
  end
end
