class CreateMerchants < ActiveRecord::Migration
  def change
    create_table :merchants do |t|

    	t.string :url, null: false
      t.string :name, null: false
      t.boolean :isMobile
      t.boolean :isActive
      t.timestamps null: false
    end
  end
end
