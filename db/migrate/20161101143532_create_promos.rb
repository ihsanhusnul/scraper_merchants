class CreatePromos < ActiveRecord::Migration
  def change
    create_table :promos do |t|

    	t.string :image_url
      t.text :description
      t.string :title
      t.datetime :expire_date
      t.integer :merchant_id, null: false

      t.timestamps null: false
    end
  end
end
